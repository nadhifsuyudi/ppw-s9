from django.apps import AppConfig


class PpwS9AppConfig(AppConfig):
    name = 'ppw_s9_app'
