from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import index
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class HomepageTestCase(TestCase):

    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_index_use_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


class FunctionalTestIndex(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
        #self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    def test_title_index(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        self.assertIn('Books', self.browser.title)

    def test_search_script(self):
        self.browser.get(self.live_server_url)
        self.browser.find_element_by_id("search").send_keys("Overlord")
        self.browser.find_element_by_id("button").click()
        time.sleep(5)
        result = self.browser.find_element_by_id("Book0")
        self.assertIn("Overlord", result.text)



