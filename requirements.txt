asgiref==3.2.3
coverage==5.0.3
Django==3.0.2
gunicorn==20.0.4
pytz==2019.3
sqlparse==0.3.0
whitenoise==5.0.1
dj-database-url==0.5.0
Pillow==7.0.0
selenium==3.4.3
psycopg2